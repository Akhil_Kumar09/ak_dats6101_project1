---
title: "Intro_to_R_project"
author: "Akhil Kumar Baipaneni"
date: "29/09/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r basicfcn, include=F}
# can add quietly=T option to the require() function
loadPkg = function(x) { if (!require(x,character.only=T, quietly =T)) { install.packages(x,dep=T,repos="http://cran.us.r-project.org"); if(!require(x,character.only=T)) stop("Package not found") } }
loadPkg('lubridate') # used from time conversions
library(lubridate)
loadPkg('dplyr') # various data transfers
library(dplyr)
loadPkg('ggplot2') #plotting and mapping
library(ggplot2)
loadPkg("BSDA")  #for z-test
library(BSDA)
loadPkg("viridis")
#loadPkg("hrbrthemes")
loadPkg('countrycode')
loadPkg('rworldmap')
```

```{r, echo=FALSE}
#Importing the marathon results data for the years 2015, 2016, 2017.
bm_2015 <- read.csv("marathon_results_2015.csv", header = TRUE)
bm_2016 <- read.csv("marathon_results_2016.csv", header = TRUE)
bm_2017 <- read.csv("marathon_results_2017.csv", header = TRUE)
```

```{r, echo=FALSE}
#Adding year column to the dataset and converting official time column into minutes and dropping the unnecessary columns
bm_2017$year <- "2017"
bm_2016$year <- "2016"
bm_2015$year <- "2015"
bm_2015$Official.Time <- as.character(bm_2015$Official.Time)
bm_2015$Official.Time.Min <- period_to_seconds(hms(bm_2015$Official.Time))/60
bm_2016$Official.Time <- as.character(bm_2016$Official.Time)
bm_2016$Official.Time.Min <- period_to_seconds(hms(bm_2016$Official.Time))/60
bm_2017$Official.Time <- as.character(bm_2017$Official.Time)
bm_2017$Official.Time.Min <- period_to_seconds(hms(bm_2017$Official.Time))/60
bm_2015 <- subset(bm_2015, select =-c(X.1,X,Proj.Time))
bm_2016 <- subset(bm_2016, select =-c(X, Proj.Time))
bm_2017 <- subset(bm_2017, select =-c(X.1, X, Proj.Time))
```

```{r, include=FALSE}
# summary of the three year's data:
summary(bm_2015)
summary(bm_2016)
summary(bm_2017)
```
### From the summary of three year's data, we can observe that the average age of the participants for all the three year's is equal.



### Now let divide the age into groups and check which age group is performing better over the years.
```{r, echo=FALSE}
bm_2017$agegroup[bm_2017$Age >= 18 & bm_2017$Age <= 24] <- '18-24'
bm_2017$agegroup[bm_2017$Age >= 25 & bm_2017$Age <= 29] <- '25-29'
bm_2017$agegroup[bm_2017$Age >= 30 & bm_2017$Age <= 34] <- '30-34'
bm_2017$agegroup[bm_2017$Age >= 35 & bm_2017$Age <= 39] <- '35-39'
bm_2017$agegroup[bm_2017$Age >= 40 & bm_2017$Age <= 44] <- '40-44'
bm_2017$agegroup[bm_2017$Age >= 45 & bm_2017$Age <= 49] <- '45-49'
bm_2017$agegroup[bm_2017$Age >= 50 & bm_2017$Age <= 54] <- '50-54'
bm_2017$agegroup[bm_2017$Age >= 55 & bm_2017$Age <= 59] <- '55-59'
bm_2017$agegroup[bm_2017$Age >= 60] <- '60+'
bm_2017$agegroup <- as.factor(bm_2017$agegroup)
agmean2017 <- aggregate(bm_2017$Official.Time.Min ~ bm_2017$agegroup, FUN=mean)
names(agmean2017) <- c('agegroup', 'avg.official.time')
ggplot(agmean2017,aes(agegroup,avg.official.time, fill=agegroup))+
        geom_col()+
        xlab("agegroup")+
        ylab("Mean official time")+
        ggtitle("Mean official time for Age groups for the year 2017")

bm_2016$agegroup[bm_2016$Age >= 18 & bm_2016$Age <= 24] <- '18-24'
bm_2016$agegroup[bm_2016$Age >= 25 & bm_2016$Age <= 29] <- '25-29'
bm_2016$agegroup[bm_2016$Age >= 30 & bm_2016$Age <= 34] <- '30-34'
bm_2016$agegroup[bm_2016$Age >= 35 & bm_2016$Age <= 39] <- '35-39'
bm_2016$agegroup[bm_2016$Age >= 40 & bm_2016$Age <= 44] <- '40-44'
bm_2016$agegroup[bm_2016$Age >= 45 & bm_2016$Age <= 49] <- '45-49'
bm_2016$agegroup[bm_2016$Age >= 50 & bm_2016$Age <= 54] <- '50-54'
bm_2016$agegroup[bm_2016$Age >= 55 & bm_2016$Age <= 59] <- '55-59'
bm_2016$agegroup[bm_2016$Age >= 60] <- '60+'
bm_2016$agegroup <- as.factor(bm_2016$agegroup)
agmean2016 <- aggregate(bm_2016$Official.Time.Min ~ bm_2016$agegroup, FUN=mean)
names(agmean2016) <- c('agegroup', 'avg.official.time')
ggplot(agmean2016,aes(agegroup,avg.official.time, fill=agegroup))+
        geom_col()+
        xlab("agegroup")+
        ylab("Mean official time")+
        ggtitle("Mean official time for Age groups for the year 2016")

bm_2015$agegroup[bm_2015$Age >= 18 & bm_2015$Age <= 24] <- '18-24'
bm_2015$agegroup[bm_2015$Age >= 25 & bm_2015$Age <= 29] <- '25-29'
bm_2015$agegroup[bm_2015$Age >= 30 & bm_2015$Age <= 34] <- '30-34'
bm_2015$agegroup[bm_2015$Age >= 35 & bm_2015$Age <= 39] <- '35-39'
bm_2015$agegroup[bm_2015$Age >= 40 & bm_2015$Age <= 44] <- '40-44'
bm_2015$agegroup[bm_2015$Age >= 45 & bm_2015$Age <= 49] <- '45-49'
bm_2015$agegroup[bm_2015$Age >= 50 & bm_2015$Age <= 54] <- '50-54'
bm_2015$agegroup[bm_2015$Age >= 55 & bm_2015$Age <= 59] <- '55-59'
bm_2015$agegroup[bm_2015$Age >= 60] <- '60+'
bm_2015$agegroup <- as.factor(bm_2015$agegroup)
agmean2015 <- aggregate(bm_2015$Official.Time.Min ~ bm_2015$agegroup, FUN=mean)
names(agmean2015) <- c('agegroup', 'avg.official.time')

ggplot(agmean2015,aes(agegroup,avg.official.time, fill=agegroup))+
        geom_col()+
        xlab("agegroup")+
        ylab("Mean official time")+
        ggtitle("Mean official time for Age groups for the year 2015")
```

From the above three plots, we can observe that 30-34 Age group has the lowest average official time over the years. So, we can conclude that 30-34 Age group people is performing well compared to other Age groups in all the years.



### Let us see if there is any trend in the Average official time for top four countries with highest number of runners over the three years.
```{r, echo=F}
off.finish.time.mean.2017 <- aggregate(bm_2017$Official.Time.Min ~ bm_2017$Country, FUN=mean)
names(off.finish.time.mean.2017) <- c('Country', 'avg.official.time')
Total.runners.2017_country <- aggregate(bm_2017$Name ~ bm_2017$Country,FUN = length)
names(Total.runners.2017_country) <- c('Country', 'Total.Runners')
countries_df_2017 <- merge(Total.runners.2017_country, off.finish.time.mean.2017, by='Country')
countries_df_2017 <- countries_df_2017[order(countries_df_2017$avg.official.time),]
countries_df_2017$year <- "2017"

country.official_time1 <- joinCountryData2Map(countries_df_2017, joinCode = "ISO3", nameJoinColumn = "Country", mapResolution="coarse")
mapCountryData(country.official_time1, nameColumnToPlot="avg.official.time",mapTitle="Average official times for countries in 2017 data", catMethod = "pretty", missingCountryCol = gray(.8), colourPalette = "heat")

off.finish.time.mean.2016 <- aggregate(bm_2016$Official.Time.Min ~ bm_2016$Country, FUN=mean)
names(off.finish.time.mean.2016) <- c('Country', 'avg.official.time')
Total.runners.2016_country <- aggregate(bm_2016$Name ~ bm_2016$Country,FUN = length)
names(Total.runners.2016_country) <- c('Country', 'Total.Runners')
countries_df_2016 <- merge(Total.runners.2016_country, off.finish.time.mean.2016, by='Country')
countries_df_2016 <- countries_df_2016[order(countries_df_2016$avg.official.time),]
countries_df_2016$year <- "2016"

country.official_time2 <- joinCountryData2Map(countries_df_2016, joinCode = "ISO3", nameJoinColumn = "Country", mapResolution="coarse")
mapCountryData(country.official_time2, nameColumnToPlot="avg.official.time",mapTitle="Average official times for countries in 2016 data", catMethod = "pretty", missingCountryCol = gray(.8), colourPalette = "heat")

off.finish.time.mean.2015 <- aggregate(bm_2015$Official.Time.Min ~ bm_2015$Country, FUN=mean)
names(off.finish.time.mean.2015) <- c('Country', 'avg.official.time')
Total.runners.2015_country <- aggregate(bm_2015$Name ~ bm_2015$Country,FUN = length)
names(Total.runners.2015_country) <- c('Country', 'Total.Runners')
countries_df_2015 <- merge(Total.runners.2015_country, off.finish.time.mean.2015, by='Country')
countries_df_2015 <- countries_df_2015[order(countries_df_2015$avg.official.time),]
countries_df_2015$year <- "2015"

country.official_time3 <- joinCountryData2Map(countries_df_2015, joinCode = "ISO3", nameJoinColumn = "Country", mapResolution="coarse")
mapCountryData(country.official_time3, nameColumnToPlot="avg.official.time",mapTitle="Average official times for countries in 2015 data", catMethod = "pretty", missingCountryCol = gray(.8), colourPalette = "heat")

country.official_time <- rbind(countries_df_2015, countries_df_2016, countries_df_2017)
country.official_time <- country.official_time[order(-country.official_time$Total.Runners),]
```

### Taking the top four countries with highest number of participants. USA, Canada, UK and Mexico has the highest number of runners in all the three years.
```{r, echo=FALSE, warning=FALSE}
tr_country <- subset(country.official_time, country.official_time$Country %in% c('USA', 'CAN', 'GBR', 'MEX'))
ggplot(tr_country, aes(x = year,y = avg.official.time, fill=year)) +
        geom_bar(position="dodge",stat="identity") +
        scale_fill_viridis(discrete = T, option = "E") +
        ggtitle("Average official time of top 4 Countries with highest number of runners") +
        facet_wrap(~Country) +
        #theme_ipsum() +
        theme(legend.position="none") +
        xlab("Year")

```

As we can see from the plot, there is very slight increase in average official time for top four countries with highest number of runners over the three years.


### Now lets look at the ANOVA test comparing official time for all the three years.
Null hypothesis: There is no difference in means of the official time of runners for three years.

```{r, echo=FALSE}
set.seed(2402)
sample_2017 <- bm_2017[sample(nrow(bm_2017),50),c('year', 'Official.Time.Min')]
sample_2016 <- bm_2016[sample(nrow(bm_2016),50),c('year', 'Official.Time.Min')]
sample_2015 <- bm_2015[sample(nrow(bm_2015),50),c('year', 'Official.Time.Min')]

official_time_sample <- rbind(sample_2017, sample_2016, sample_2015)
anoveRes_official_racing_time <- aov(Official.Time.Min ~ year, data=official_time_sample)
summary_official_time1 <- unlist(summary(anoveRes_official_racing_time))
tuckeyageaov <- TukeyHSD(anoveRes_official_racing_time)
tuckeyageaov
boxplot(Official.Time.Min ~ year, data = official_time_sample, xlab="Year", ylab="Official time in minutes", main="official time of runners over 3 years", col=c("#87ceeb", "#FFA500", "#ffc0cb"))
```

p value `r summary_official_time1["Pr(>F)1"]` is greater than 0.05. Also from tuckey's results, we can see the probability value is greater than 0.05 for all comparisons. So, we fail to reject the null hypothesis and we can conclude that the means of the official time is same for three years.


### Now let's see whether there are any runners participated in all the three years.
```{r, echo=FALSE}
bm_2015$Age_compare_2015_2016 <- bm_2015$Age+1
bm_2016$Age_compare_2015_2016 <- bm_2016$Age
both <- merge(bm_2015, bm_2016, by=c("Name","Age_compare_2015_2016"))
both$Age_compare_2016_2017 <- both$Age_compare_2015_2016+1
bm_2017$Age_compare_2016_2017 <- bm_2017$Age
both1 <- merge(bm_2017, both, by=c("Name","Age_compare_2016_2017"))
```
There are total `r nrow(both)` runners participated in both 2015 and 2016 marathon and there are `r nrow(both1)` runners participated in all the three years.

### Now let's check whether the performance of the runners participated in all the three years is better than the first time runners by comparing the average official time in the year 2017.
Null Hypothesis for z-Test: No difference in average official times of first time and third time runners.

Alternate Hypothesis: Average official time of first time runners is greater than Average Official time of runners participated in all the three years.
alpha:0.05
```{r, echo=FALSE}
rem_runners.2017 <- bm_2017[!bm_2017$Bib %in% both1$Bib,]
ztest95 = z.test(x=rem_runners.2017$Official.Time.Min,alternative = "greater" ,mu=mean(both1$Official.Time.Min), sigma.x = sd(both1$Official.Time.Min))
ztest95
```
Since the p-value less than alpha 0.05, we reject the null hypothesis.That means the average official time of third time runners is lesser than first time runners. we can conclude that third time runners performance is better than the first time runners.